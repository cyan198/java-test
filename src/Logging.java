import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Logging {

	public static void main(String[] args) throws SecurityException, IOException {
		Logger logger = Logger.getLogger("myLogger");
		FileHandler fileLog = new FileHandler("mylog.log");
		logger.addHandler(fileLog);

		logger.log(Level.INFO, "i'm here to Info!");
		logger.log(Level.SEVERE, "i'm here to severe!");
	}
}
