import static org.junit.Assert.*;

import org.junit.Test;

public class UnitTest2 {

	@Test
	public void test1() {
		int[] A = { 1, 1, 1, 5, 50};
		int p = Codility.solution3(A);
		assertEquals(1, p);
	}
	@Test
	public void test2() {
		int[] A = { 2, 2, 2, 2, 2, 3, 4, 4, 4, 6};
		int p = Codility.solution3(A);
		assertEquals(-1, p);
	}

	@Test
	public void test3() {
		int[] A = { 1, 1, 1, 1, 50};
		int p = Codility.solution3(A);
		assertEquals(1, p);
	}

	@Test
	public void test4() {
		int[] A = { 1, 1, 2};
		int p = Codility.solution3(A);
		assertEquals(1, p);
	}

}
