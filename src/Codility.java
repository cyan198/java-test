
public class Codility {

	public static int solution(int[] A) {
		int res = -1;
		int p = 0;
		for (p = 0; p < A.length;p++) {
			int sum1 = 0;
			int sum2 = 0;
			for (int i=0;i <A.length;i++) {
				if (p > i) {
					sum1+=A[i];
				}
				if (p < i) {
					sum2+=A[i];
				}
			}
			System.out.println(p);
			System.out.println(sum1 + "::" + sum2);
			if (sum1 == sum2) {
				res = p;
				break;
			}
		}
        return res;

    }

	static int solution3(int[] A) {
        int n = A.length;
        int[] L = new int[n + 1];
        L[0] = -1;
        for (int i = 0; i < n; i++) {
            L[i + 1] = A[i];
        }
        int count = 0;
        int pos = (n + 1) / 2;
        int candidate = L[pos];
        for (int i = 1; i <= n; i++) {
            if (L[i] == candidate)
                count = count + 1;
        }
        if ((n%2 == 0 && count > pos) || (n%2 != 0 && count > pos-1))
//        if (count > pos)
            return candidate;
        return (-1);
    }
}
